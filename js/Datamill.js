/**
 * JavaScript Datamill client.
 */

import('/src/JSUtils/util/File.js');
import('/src/JSUtils/util/Request.js');

/**
 * @class
 */
class Datamill {

    /**
     * @public
     */
    constructor(organizationId, apiKey, user) {
        this._organization = {
            'id': organizationId,
        };
        this._apiKey = apiKey;
        this._user = user;
    }

    /**
     * @public
     */
    setAPIKey(apiKey) {
        this._apiKey = apiKey;

        return this;
    }

    /**
     * @public
     */
    setUser(user) {
        this._user = user;

        return this;
    }

    /**
     * @private
     */
    _post(data) {
        return Request.post(
            this.constructor._ENDPOINT,
            data,
            null,
            true,
        );
    }

    /**
     * @private
     */
    _invoke(functionName, method, args, kwargs) {
        kwargs.organization = this._organization;
        kwargs.apiKey = this._apiKey;
        kwargs.user = this._user;

        const data = {
            'FunctionName': functionName,
            'Payload': JSON.stringify({
                'method': method,
                'args': args,
                'kwargs': kwargs,
            }),
        };

        return this._post(data).then(result => {
            result = JSON.parse(result);

            if (result && result.errorMessage) {
                return Promise.reject(`Error while invoking ${functionName}.${method}: ${result.errorMessage}`);
            }

            return result;
        });
    }

    /**
     * @public
     */
    listFiles(prefix) {
        return this._invoke(
            'StorageService',
            'default.list',
            [],
            {
                prefix,
            },
        );
    }

    /**
     * @public
     */
    getFile(filePath) {
        return this._invoke(
            'StorageService',
            'default.get',
            [],
            {filePath},
        );
    }

    /**
     * @public
     */
    deleteFile(filePath) {
        return this._invoke(
            'StorageService',
            'default.delete',
            [],
            {filePath},
        );
    }

    /**
     * @public
     */
    downloadFile(filePath) {
        this.getFile(filePath).then(data => {
            const filename = file.path.split('/').reverse()[0];
            let link = document.createElement('a');

            link.href = 'data:text/octet-stream;base64,' + data;
            link.download = filename;

            link.click();
        });
    }

    /**
     * @public
     */
    uploadFile(fileInput, filePath, fileExtension) {
        filePath = filePath || null;

        if (filePath) {
            filePath = Array.from(filePath).filter(char =>
                Datamill._VALID_FILENAME_CHARS.indexOf(char) > -1).join('');
        }

        return File.upload(fileInput, this.constructor._ENDPOINT).then(uploadedFileName => {
            return this._invoke(
                'StorageService',
                'default.write',
                [],
                {
                    uploadedFileName,
                    filePath,
                    fileExtension
                },
            );
        });
    }

    /**
     * @public
     * Returns a file path containing only the allowed characters.
     */
    cleanFilePath(path) {
        return Array.from(path)
            .filter(char => this.constructor._VALID_FILENAME_CHARS.indexOf(char) > -1)
            .join('');
    }

    /**
     * @public
     */
    listItemTypes(limit, offset) {
        return this._invoke(
            'ItemClassService',
            'default.list',
            [],
            {limit, offset},
        );
    }

    /**
     * @private
     */
    _applyItemProperties(item) {
        let result = {};

        if (item.itemProperties) {
            item.itemProperties.forEach(prop => {
                result[prop.itemClassPropertyName] = prop.propertyValue;
            });
        }

        Object.assign(result, {
            id: item.id,
            itemClassId: item.itemClassId,
            organizationId: item.organizationId,
            itemProperties: item.itemProperties,
        });

        return new Datamill.Item(result);
    }

    /**
     * @public
     */
    listItems(itemType, limit, offset, constraints) {
        return this._invoke(
            'ItemsService',
            'default.list',
            [itemType.id],
            {limit, offset, constraints},
        ).then(items => {
            return items.map(this._applyItemProperties);
        });
    }

    /**
     * @public
     * @param {Datamill.Item} item
     * @return {Promise}
     */
    writeItem(item) {
        if (!item instanceof Datamill.Item) {
            throw new Error('item must be an instance of Datamill.Item.');
        }

        return this._invoke(
            'ItemsService',
            'default.write',
            [item],
            {},
        );
    }

    /**
     * @public
     */
    getItem(itemId) {
        return this._invoke(
            'ItemsService',
            'default.get',
            [itemId],
            {},
        ).then(item => {
            return this._applyItemProperties(item);
        });
    }
}

Datamill._ENDPOINT = 'https://compute-us-west-1.johndsutton.com/';
Datamill._VALID_FILENAME_CHARS = '._abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789/';

/**
 * @class Datamill.Item
 */
Datamill.Item = class {

    /**
     * @public
     */
    constructor(item) {
        Object.assign(this, item);
    }

    /**
     * @public
     */
    setProperty(propertyName, value) {
        for (let prop of this.itemProperties) {
            if (prop.itemClassPropertyName === propertyName) {
                prop.propertyValue = value;

                return;
            }
        }
    }

    /**
     * @public
     */
    getProperty(propertyName) {
        for (let prop of this.itemProperties) {
            if (prop.itemClassPropertyName === propertyName)
                return prop.propertyValue;
        }
    }
}