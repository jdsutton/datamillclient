# Datamill Client

## API Usage
POST https://compute-us-west-1.johndsutton.com

content-type: application/x-www-form-urlencoded

```json
{
    "FunctionName": "NameOfService",
    "Payload": {
        "method":"method.name",
        "args": [...],
        "kwargs":{
            ...,
            "apikey": "...",
            "organization": {
                "id": "..."
            }
        }
    }
}
```